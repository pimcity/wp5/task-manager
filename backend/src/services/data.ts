/* eslint-disable import/prefer-default-export */
import { whitelistedUsers } from '../db';
import { WhitelistedUser } from '../db/models';
import { DataSafe, DataUser } from '../externals/definitions';
import toObject from '../utils/toObject';

const toCheckedUser = (
  userId: string, allWhiteListedUsers: Record<string, WhitelistedUser>,
) => ({ id: userId, hasMinimumData: !!allWhiteListedUsers[userId] });

const checkData = (user?: DataUser) => (
  user?.personalInformation?.firstName && user.personalInformation?.lastName
  && user.personalInformation?.country && user.personalInformation?.birthDate
);

export const requireMinimumData = async (
  dataSafe: DataSafe, qualifiedUsers: Array<string>,
): Promise<Array<string>> => {
  const allWhiteListedUsers = toObject(await whitelistedUsers.list(), 'id');
  const checkedUsers = qualifiedUsers.map((u) => toCheckedUser(u, allWhiteListedUsers));
  const unknownUsers = checkedUsers.filter((u) => !u.hasMinimumData).map((u) => u.id);
  const usersMap = await dataSafe.getDataMap(unknownUsers, ['personal-information']);

  return checkedUsers.filter((user) => {
    if (user.hasMinimumData) return true;
    const hasMinimumData = checkData(usersMap[user.id]);
    if (hasMinimumData) whitelistedUsers.store({ id: user.id });
    return hasMinimumData;
  }).map((user) => user.id);
};
