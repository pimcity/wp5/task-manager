import { Periodicity } from '../../db/models';
import { Points } from '../../externals/CreditsManager';

const periodToPoints: Record<Periodicity, keyof Points> = {
  'one-time': 'allTime',
  daily: 'lastDay',
  weekly: 'lastWeek',
  monthly: 'lastMonth',
  yearly: 'lastYear',
};

const getPointsNeeded = (
  pointsNeeded: number,
  periodicity: Periodicity,
  points: Points,
) => Math.max(0, pointsNeeded - points[periodToPoints[periodicity]]);

export default getPointsNeeded;
