import format from 'date-fns/format';

import { Periodicity, Sweepstake, Winner } from '../../db/models';
import { Points } from '../../externals/CreditsManager';
import { SweepstakeDTO } from '../../types/SweepstakeDTO';

import { getNextExecution } from './getExecution';
import getPointsNeeded from './getPointsNeeded';

const getDates = (periodicity: Periodicity, nextExecution?: number, previousExecution?: number) => {
  const now = Date.now();
  const formatStr = 'dd/MM/yyyy';
  const nextDate = nextExecution ? format(nextExecution, formatStr) : '-';

  if (nextExecution && now > nextExecution) {
    const followingExecution = getNextExecution(periodicity, nextExecution);
    return {
      previousDate: nextDate,
      date: followingExecution ? format(followingExecution, formatStr) : nextDate,
    };
  }
  return {
    previousDate: previousExecution ? format(previousExecution, formatStr) : '-',
    date: nextDate,
  };
};

const getLastWinner = (winners: Array<Winner>) => (
  winners.length > 0 ? winners[winners.length - 1] : undefined
);

const toSweepstakeDTO = (
  userId: string,
  points: Points,
  { nextExecution, winners, ...s }: Sweepstake,
): SweepstakeDTO => {
  const lastWinner = getLastWinner(winners);
  return {
    ...s,
    pointsNeeded: nextExecution ? getPointsNeeded(s.pointsNeeded, s.periodicity, points) : 0,
    ...getDates(s.periodicity, nextExecution, lastWinner?.executionDate),
    lastWinner: lastWinner?.userIds[0],
    won: lastWinner ? lastWinner.userIds.includes(userId) : undefined,
  };
};

export default toSweepstakeDTO;
