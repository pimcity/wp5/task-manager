import initSweepstakesJson from '../../constants/initSweepstakes.json';

import { sweepstakes as sweepstakesStore } from '../../db';
import { Sweepstake, SweepstakeInit } from '../../db/models';
import { getPoints, getUsersAboveThreshold } from '../../externals/CreditsManager';
import { DataSafe } from '../../externals/definitions';
import { SweepstakeDTO } from '../../types/SweepstakeDTO';
import toObject from '../../utils/toObject';

import { requireMinimumData } from '../data';

import { getExecutionInterval, getNextExecution } from './getExecution';
import toSweepstakeDTO from './toSweepstakeDTO';

export const populateSweepstakes = async () => {
  const initSweepstakes = initSweepstakesJson as Array<SweepstakeInit>;
  const initSweepstakesMap = toObject(initSweepstakes, 'id');

  const existingSweepstakes = await sweepstakesStore.list();
  const existingSweepstakesMap = toObject(existingSweepstakes, 'id');

  const sweepstakesToStore: Array<Sweepstake> = initSweepstakes
    .map((s) => ({
      ...s,
      winners: existingSweepstakesMap[s.id]?.winners || [],
      nextExecution: !s.nextExecution || s.resetExecution || !existingSweepstakesMap[s.id]
        ? s.nextExecution
        : existingSweepstakesMap[s.id]?.nextExecution,
    }));

  const sweepstakesToDisable = existingSweepstakes
    .filter((e) => !initSweepstakesMap[e.id])
    .map((e) => ({ ...e, disabled: true }));

  await sweepstakesStore.batchStore([...sweepstakesToStore, ...sweepstakesToDisable]);
};

export const getSweepstakes = async (
  userId: string, token: string,
): Promise<Array<SweepstakeDTO>> => {
  const existingSweepstakes = await sweepstakesStore.list();
  const points = await getPoints(token);

  return existingSweepstakes
    .filter((e) => !e.disabled)
    .map((s) => toSweepstakeDTO(userId, points, s));
};

export const getUsersForSweepstake = async (sweepstakeId: string, dataSafe: DataSafe) => {
  const sweepstake = await sweepstakesStore.fetch(sweepstakeId);
  if (!sweepstake.nextExecution) {
    return {
      sweepstake,
      interval: { from: 0, to: Date.now() },
      users: [],
    };
  }

  const [from, to] = getExecutionInterval(sweepstake.periodicity, sweepstake.nextExecution);
  const qualifiedUsers = await getUsersAboveThreshold(sweepstake.pointsNeeded, from, to);
  const users = await requireMinimumData(dataSafe, qualifiedUsers);
  return {
    sweepstake,
    interval: { from, to },
    users,
  };
};

export const pickWinners = async (
  sweepstakeId: string, userIds: Array<string>,
): Promise<Sweepstake> => {
  const sweepstake = await sweepstakesStore.fetch(sweepstakeId);
  if (!sweepstake.nextExecution) throw new Error('No execution was scheduled');

  const nextExecution = getNextExecution(sweepstake.periodicity, sweepstake.nextExecution);

  const [updatedSweepstake] = await Promise.all([
    sweepstakesStore.upsert(sweepstakeId, ({
      nextExecution,
      winners: [
        ...sweepstake.winners,
        {
          executionDate: sweepstake.nextExecution,
          userIds,
        },
      ],
    })),
    !nextExecution
      ? sweepstakesStore.removeProps(sweepstakeId, { nextExecution: true }) : undefined,
  ]);
  return updatedSweepstake;
};

export const deleteLastWinners = async (sweepstakeId: string): Promise<Sweepstake> => {
  const sweepstake = await sweepstakesStore.fetch(sweepstakeId);
  if (sweepstake.winners.length === 0) return sweepstake;

  const last = sweepstake.winners[sweepstake.winners.length - 1];

  return sweepstakesStore.upsert(sweepstakeId, ({
    nextExecution: last.executionDate,
    winners: sweepstake.winners.slice(0, sweepstake.winners.length - 1),
  }));
};
