import addDays from 'date-fns/addDays';
import addWeeks from 'date-fns/addWeeks';
import addYears from 'date-fns/addYears';
import startOfDay from 'date-fns/startOfDay';
import startOfMonth from 'date-fns/startOfMonth';
import startOfWeek from 'date-fns/startOfWeek';
import startOfYear from 'date-fns/startOfYear';
import subDays from 'date-fns/subDays';
import subMonths from 'date-fns/subMonths';
import subWeeks from 'date-fns/subWeeks';
import subYears from 'date-fns/subYears';
import toDate from 'date-fns/toDate';

import { Periodicity } from '../../db/models';

type ModDate = (date: number | Date, amount: number) => Date;
type ChangeDate = (date: number | Date) => Date;

const addDates: Record<Periodicity, ModDate | undefined> = {
  'one-time': undefined,
  daily: addDays,
  weekly: addWeeks,
  monthly: (date, amount) => {
    let result = toDate(date);
    for (let i = 0; i < amount; i += 1) {
      const weeks = addWeeks(result, 4).getMonth() > result.getMonth() ? 4 : 5;
      result = addWeeks(result, weeks);
    }
    return result;
  },
  yearly: addYears,
};

const subDates: Record<Periodicity, ModDate | undefined> = {
  'one-time': undefined,
  daily: subDays,
  weekly: subWeeks,
  monthly: (date, amount) => {
    let result = toDate(date);
    for (let i = 0; i < amount; i += 1) {
      const weeks = subWeeks(result, 4).getMonth() < result.getMonth() ? 4 : 5;
      result = subWeeks(result, weeks);
    }
    return result;
  },
  yearly: subYears,
};

const intervals: Record<Periodicity, { fromFn?: ModDate, toFn?: ChangeDate }> = {
  'one-time': {},
  daily: { fromFn: subDays, toFn: startOfDay },
  weekly: { fromFn: (d, a) => addDays(subWeeks(d, a), 1), toFn: startOfWeek },
  monthly: { fromFn: subMonths, toFn: startOfMonth },
  yearly: { fromFn: subYears, toFn: startOfYear },
};

const getExecution = (
  mods: Record<Periodicity, ModDate | undefined>,
  periodicity: Periodicity,
  lastExecution: number,
): number | undefined => {
  const modDate = mods[periodicity];
  return modDate ? modDate(lastExecution, 1).getTime() : undefined;
};

export const getNextExecution = (
  periodicity: Periodicity, lastExecution: number,
): number | undefined => getExecution(addDates, periodicity, lastExecution);

export const getPreviousExecution = (
  periodicity: Periodicity, lastExecution: number,
): number | undefined => getExecution(subDates, periodicity, lastExecution);

export const getExecutionInterval = (
  periodicity: Periodicity, nextExecution: number = Date.now(),
): [number, number] => {
  const { fromFn, toFn } = intervals[periodicity];
  const to = toFn ? toFn(nextExecution).getTime() : nextExecution;
  const from = fromFn ? fromFn(to, 1).getTime() : 0;
  return [from, to];
};
