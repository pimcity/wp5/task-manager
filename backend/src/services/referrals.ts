import { v4 as uuidv4 } from 'uuid';

import config from '../config';
import { referrers as referrersStore, referees as refereesStore } from '../db';
import { Referee } from '../db/models';
import { getPointsForUser } from '../externals/CreditsManager';
import { ConflictError, NotFoundError } from '../utils/error';
import logger from '../utils/logger';

import { markTaskAsCompleted } from './tasks';

export const getOrCreateReferralCode = async (userId: string) => {
  const referrers = await referrersStore.queryByIndex('userId', userId);
  if (referrers.length > 0) return { referralCode: referrers[0].code };

  const code = uuidv4();
  await referrersStore.store({ userId, code });
  return { referralCode: code };
};

export const useReferral = async (refereeId: string, referrerCode: string) => {
  const referee = await refereesStore.safeFetch(refereeId);
  if (referee) throw new ConflictError('User has already used a referrer code');

  const referrer = await referrersStore.safeFetch(referrerCode);
  if (!referrer) throw new NotFoundError('Referrer does not exist');
  const { userId: referrerId } = referrer;
  if (refereeId === referrerId) throw new ConflictError('User cannot refer himself/herself');

  await refereesStore.store({ userId: refereeId, code: referrerCode, referrerId });
};

const applyReferral = async (referee: Referee) => {
  const { referralActivated } = await refereesStore.upsert(
    referee.userId,
    { referralActivated: true },
    { returnValues: 'ALL_OLD' },
  );
  if (referralActivated) return;
  try {
    await markTaskAsCompleted('invite-friends', referee.referrerId);
  } catch (err) {
    logger.error(`Cannot mark invite-friends task for user: ${referee.referrerId}. ${err}`);
  }
};

export const checkReferral = async (userId: string): Promise<void> => {
  const referee = await refereesStore.safeFetch(userId);
  if (!referee) return;
  const { allTime } = await getPointsForUser(referee.userId);

  if (allTime >= config.referrals.pointsThreshold) {
    await applyReferral(referee);
  }
};

export const deleteUserReferences = async (userId: string) => {
  const referrers = await referrersStore.queryByIndex('userId', userId);
  return Promise.all([
    ...referrers.map((r) => referrersStore.delete(r.code)),
    refereesStore.delete(userId),
  ]);
};
