import startOfDay from 'date-fns/startOfDay';
import startOfMonth from 'date-fns/startOfMonth';
import startOfWeek from 'date-fns/startOfWeek';
import startOfYear from 'date-fns/startOfYear';

import availableTasksJson from '../constants/availableTasks.json';
import tasksTexts from '../constants/tasksTexts.json';

import { tasks as tasksStore } from '../db';
import { AvailableTask, Periodicity, Task } from '../db/models';
import { registerTransaction } from '../externals/CreditsManager';
import { TaskDTO } from '../types/TaskDTO';
import toObject from '../utils/toObject';

const availableTasks = (availableTasksJson as Array<AvailableTask>);
const availableTasksMap = toObject(availableTasks, 'taskId');

const buildId = (taskId: string, userId: string) => `${taskId}-${userId}`;

const getTaskText = (taskId: string, text: 'NAME' | 'DESCRIPTION'): string => {
  const key = `TASKS.${taskId.toUpperCase()}.${text}`;
  // @ts-ignore
  return tasksTexts[key] || key;
};

type StartOfDate = (date: number | Date) => number;

const completionCondition: Record<Periodicity, StartOfDate> = {
  'one-time': () => 0,
  daily: (date) => startOfDay(date).getTime(),
  weekly: (date) => startOfWeek(date, { weekStartsOn: 1 }).getTime(),
  monthly: (date) => startOfMonth(date).getTime(),
  yearly: (date) => startOfYear(date).getTime(),
};

const isCompleted = (now: number, periodicity: Periodicity, storedTask?: Task) => {
  if (
    !storedTask
    || storedTask.completedAt === undefined
    || storedTask.completedAt === null
  ) return false;

  const startingPoint = completionCondition[periodicity](now);
  return storedTask.completedAt >= startingPoint;
};

export const getTasks = async (userId: string): Promise<Array<TaskDTO>> => {
  const now = Date.now();
  const tasks = await tasksStore.queryByIndex('userId', userId);
  const tasksMap = toObject(tasks, 'taskId');

  return availableTasks
    .filter((task) => !task.disabled)
    .map((task) => {
      const storedTask = tasksMap[task.taskId];
      const completed = isCompleted(now, task.periodicity, storedTask);
      return ({
        id: task.taskId,
        name: getTaskText(task.taskId, 'NAME'),
        description: getTaskText(task.taskId, 'DESCRIPTION'),
        completed,
        completedAt: completed ? storedTask?.completedAt : undefined,
        points: task.points,
        periodicity: task.periodicity,
        repetitions: completed ? storedTask.repetitions : 0,
        maxRepetitions: task.maxRepetitions,
      });
    });
};

const markTaskAsCompletedForAUser = async (
  taskId: string, userId: string, now: number,
): Promise<void> => {
  const availableTask = availableTasksMap[taskId];
  if (!availableTask) throw new Error('Task not available');
  if (availableTask.disabled) return;

  const taskDbId = buildId(taskId, userId);
  const task = await tasksStore.safeFetch(taskDbId);
  const completed = isCompleted(now, availableTask.periodicity, task);
  if (
    completed && availableTask.maxRepetitions !== 'unlimited'
    && task?.repetitions && task.repetitions >= availableTask.maxRepetitions
  ) return;

  await tasksStore.upsert(taskDbId, {
    id: taskDbId,
    taskId,
    userId,
    repetitions: task && completed ? task.repetitions + 1 : 1,
    completedAt: now,
  });

  try {
    const taskName = getTaskText(taskId, 'NAME');
    await registerTransaction(userId, taskId, taskName, availableTask.points, now);
  } catch (err) {
    // if it fails, rollback
    if (!task) await tasksStore.delete(taskDbId);
    else {
      await tasksStore.upsert(taskDbId, task);
    }
    throw err;
  }
};

export const markTaskAsCompleted = async (
  taskId: string, userIds: string | Array<string>,
): Promise<void> => {
  const now = Date.now();
  const theUserIds = Array.isArray(userIds) ? userIds : [userIds];
  const proms = theUserIds.map((userId) => markTaskAsCompletedForAUser(taskId, userId, now));
  await Promise.all(proms);
};

export const deleteUserTasks = async (userId: string) => {
  const tasks = await tasksStore.queryByIndex('userId', userId);
  return Promise.all(tasks.map((t) => tasksStore.delete(t.id)));
};
