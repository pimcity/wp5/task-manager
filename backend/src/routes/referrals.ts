import { Request } from 'express';
import Router from 'express-promise-router';

import { getKeycloak } from '../externals/Keycloak';
import decorateUserRequest from '../middlewares/decorateUserRequest';
import { run } from '../middlewares/run';
import { checkReferral, getOrCreateReferralCode, useReferral } from '../services/referrals';
import { AuthRequest } from '../types/middlewares/auth';
import { NotProcessableError } from '../utils/error';

const keycloak = getKeycloak();

const router = Router();

router.get(
  '/',
  keycloak.protect('end-user'),
  decorateUserRequest,
  run(getOrCreateReferralCode, {
    params: (req: AuthRequest) => [req.user.id],
  }),
);

router.post(
  '/',
  keycloak.protect('end-user'),
  decorateUserRequest,
  run(
    async (userId: string, referrerCode?: string) => {
      if (!referrerCode) throw new NotProcessableError('Missing referrerCode');
      return useReferral(userId, referrerCode);
    }, {
      params: (req: AuthRequest) => [req.user.id, req.body.referrerCode],
    },
  ),
);

router.post(
  '/check',
  run(
    checkReferral,
    {
      params: (req: Request) => [req.body.eventData.userId],
    },
  ),
);

export default router;
