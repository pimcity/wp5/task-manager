import { Request } from 'express';
import Router from 'express-promise-router';

import decorateUserRequest from '../middlewares/decorateUserRequest';
import validateInternalAuthentication from '../middlewares/internalAuth';
import { run } from '../middlewares/run';
import { getTasks, markTaskAsCompleted } from '../services/tasks';
import { getKeycloak } from '../externals/Keycloak';
import { AuthRequest } from '../types/middlewares/auth';
import { NotProcessableError } from '../utils/error';

const keycloak = getKeycloak();

const router = Router();

router.get('/', keycloak.protect('end-user'), decorateUserRequest, run(getTasks, {
  params: (req: AuthRequest) => [req.user.id],
}));

router.post('/', validateInternalAuthentication, run(
  async (taskId?: string, userId?: string, userIds?: Array<string>) => {
    if (!taskId || !(userId || userIds)) throw new NotProcessableError('Missing taskId or userId(s)');
    const theUserIds = userId ? [userId, ...(userIds || [])] : userIds as string[];
    await markTaskAsCompleted(taskId, theUserIds);
    if (taskId === 'upload-browsing-data') await markTaskAsCompleted('plugin-installation', theUserIds);
  }, {
    params: (req: Request) => [req.body.taskId, req.body.userId, req.body.userIds],
  },
));

export default router;
