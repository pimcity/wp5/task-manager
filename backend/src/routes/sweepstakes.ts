import Router from 'express-promise-router';

import { getKeycloak } from '../externals/Keycloak';
import decorateUserRequest from '../middlewares/decorateUserRequest';
import { run } from '../middlewares/run';
import { getSweepstakes } from '../services/sweepstakes';
import { AuthRequest } from '../types/middlewares/auth';

const keycloak = getKeycloak();

const router = Router();

router.get(
  '/',
  keycloak.protect('end-user'),
  decorateUserRequest,
  run(getSweepstakes, {
    params: (req: AuthRequest) => [req.user.id, req.user.token],
  }),
);

export default router;
