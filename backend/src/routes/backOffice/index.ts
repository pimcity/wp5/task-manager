import Router from 'express-promise-router';

import { getKeycloak } from '../../externals/Keycloak';
import decorateUserRequest from '../../middlewares/decorateUserRequest';

import sweepstakes from './sweepstakes';
import users from './users';

const keycloak = getKeycloak();

const router = Router();
router.use(keycloak.protect('task-manager-admin'), decorateUserRequest);
router.use('/sweepstakes', sweepstakes);
router.use('/users', users);

export default router;
