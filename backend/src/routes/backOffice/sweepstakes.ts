import { Request, Response, NextFunction } from 'express';
import Router from 'express-promise-router';
import format from 'date-fns/format';

import { sweepstakes as sweepstakesStore } from '../../db';
import { sendPDF } from '../../externals/PDFCreator';
import PDS from '../../externals/PDS';
import { runSafe } from '../../middlewares/run';
import { deleteLastWinners, getUsersForSweepstake, pickWinners } from '../../services/sweepstakes';

import { lpad } from '../../utils/numbers';

const dataSafe = new PDS();

const router = Router();

router.get('/', runSafe(
  async () => {
    const sweepstakes = await sweepstakesStore.list();
    return sweepstakes.filter((s) => !s.disabled);
  },
));

router.get(
  '/:sweepstakeId',
  runSafe(
    (req: Request) => sweepstakesStore.fetch(req.params.sweepstakeId),
  ),
);

router.get(
  '/participants/:sweepstakeId',
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { sweepstakeId } = req.params;
      const { sweepstake, interval, users } = await getUsersForSweepstake(sweepstakeId, dataSafe);

      const fromStr = interval.from > 0 ? format(interval.from, 'dd/MM/yyyy') : 'Launch Date';
      const intervalStr = sweepstake.nextExecution
        ? `from ${fromStr} to ${format(interval.to, 'dd/MM/yyyy')}`
        : '';
      const table = {
        title: `${sweepstake.description} (${intervalStr})`,
        subtitle: `Points Needed: ${sweepstake.pointsNeeded}\nPrize: ${sweepstake.prize}`,
        headers: ['USER ID', 'SWEEPSTAKE NUMBER'],
        rows: users.map((u, i) => [u, lpad(i, 3)]),
      };
      await sendPDF(table, res);
    } catch (err) {
      next(err);
    }
  },
);

router.post(
  '/winners/:sweepstakeId',
  runSafe(
    (req: Request) => pickWinners(req.params.sweepstakeId, req.body.winners),
  ),
);

router.delete(
  '/winners/:sweepstakeId',
  runSafe(
    (req: Request) => deleteLastWinners(req.params.sweepstakeId),
  ),
);

export default router;
