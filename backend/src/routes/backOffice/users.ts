import { Request } from 'express';
import Router from 'express-promise-router';

import { deleteUserReferences } from '../../services/referrals';
import { deleteUserTasks } from '../../services/tasks';

import { runSafe } from '../../middlewares/run';

const router = Router();

router.delete(
  '/:userId',
  runSafe(
    (req: Request) => {
      const { userId } = req.params;
      return Promise.all([
        deleteUserTasks(userId),
        deleteUserReferences(userId),
      ]);
    },
  ),
);

export default router;
