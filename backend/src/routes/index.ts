import Router from 'express-promise-router';
import session from 'express-session';

import { getKeycloak, getSessionStore } from '../externals/Keycloak';

import health from './health';
import referrals from './referrals';
import sweepstakes from './sweepstakes';
import backOffice from './backOffice';
import tasks from './tasks';

const keycloak = getKeycloak();
const store = getSessionStore();

const router = Router();

router.use(session({
  secret: 'some secret',
  resave: false,
  saveUninitialized: true,
  store,
}));
router.use(keycloak.middleware());

router.use('/health', health);
router.use('/tasks', tasks);
router.use('/referrals', referrals);
router.use('/sweepstakes', sweepstakes);
router.use('/back-office', backOffice);

export default router;
