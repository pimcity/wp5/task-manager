const isObject = (value: any) => typeof value === 'object' && !Array.isArray(value) && value !== null;

export default isObject;
