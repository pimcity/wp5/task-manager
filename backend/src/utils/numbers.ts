/* eslint-disable import/prefer-default-export */
export const lpad = (value: number, padding: number): string => {
  const zeroes = new Array(padding + 1).join('0');
  return (zeroes + value).slice(-padding);
};
