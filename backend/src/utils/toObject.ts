const toObject = <K extends string, T = unknown>(array: Array<T>, key: keyof T): Record<K, T> => {
  const obj = array.reduce(
    (acc, item) => ({
      ...acc,
      [item[key] as unknown as K]: item,
    }),
    {} as Record<K, T>,
  );

  return obj;
};

export default toObject;
