/* eslint-disable import/prefer-default-export */
import { Response } from 'express';

import PDFDocument, { Table } from 'pdfkit-table';

export const sendPDF = async (table: Table, res: Response) => {
  const doc = new PDFDocument({ bufferPages: true });
  await doc.table(table);
  doc.pipe(res);
  doc.end();
  res.setHeader('Content-Type', 'application/pdf');
};
