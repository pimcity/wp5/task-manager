/* eslint-disable import/prefer-default-export */
import fetch from 'node-fetch';

import config from '../config';

export const registerTransaction = async (
  userId: string, taskId: string, taskName: string, points: number, createdAt: number,
) => {
  const res = await fetch(
    `${config.services.CreditsManager.url.replace(/\/$/, '')}/market/end-users/transactions/${userId}/task`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-API-Key': config.services.CreditsManager.apiKey,
      },
      body: JSON.stringify({
        taskId,
        taskName,
        points,
        createdAt,
      }),
    },
  );
  if (res.status >= 400) throw new Error(`${res.status} - ${res.statusText}`);
};

export interface Points {
  allTime: number,
  lastDay: number;
  lastWeek: number;
  lastMonth: number;
  lastYear: number;
}

export const getPoints = async (token: string): Promise<Points> => {
  const res = await fetch(
    `${config.services.CreditsManager.url.replace(/\/$/, '')}/accounts/end-users/points`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    },
  );
  return res.json();
};

export const getPointsForUser = async (userId: string): Promise<Points> => {
  const res = await fetch(
    `${config.services.CreditsManager.url.replace(/\/$/, '')}/accounts/end-users/points/${userId}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-API-Key': config.services.CreditsManager.apiKey,
      },
    },
  );
  return res.json();
};

export const getUsersAboveThreshold = async (
  threshold: number, from: number, to: number,
): Promise<Array<string>> => {
  const res = await fetch(
    `${config.services.CreditsManager.url.replace(/\/$/, '')}/accounts/end-users/points/list//${threshold}/${from}/${to}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-API-Key': config.services.CreditsManager.apiKey,
      },
    },
  );
  return res.json();
};
