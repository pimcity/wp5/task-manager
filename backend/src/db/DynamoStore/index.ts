import DynamoDB from 'aws-sdk/clients/dynamodb';
import { AWSError } from 'aws-sdk/lib/error';
import { PromiseResult } from 'aws-sdk/lib/request';
import { DeepMap, DeepPartial } from '../../types/miscellaneous';
import {
  docClient,
  upsertTable,
  query,
  scan,
  createUpdateExpressions,
  createRemovePropsExpressions,
  createQueryExpressions,
  primaryKeySchemaDefault,
  attributeDefinitionsDefault,
  createIncrementValuesExpressions,
} from './aux';

export interface UpsertOptions {
  returnValues: 'NONE'|'ALL_OLD'|'UPDATED_OLD'|'ALL_NEW'|'UPDATED_NEW';
}

/**
 * A key-value store that uses DynamoDB
 */
export default class DynamoStore<T> {
  private tableName: DynamoDB.TableName;

  private primaryKeySchema: DynamoDB.KeySchema;

  private attributeDefinitions: DynamoDB.AttributeDefinitions;

  private globalSecondaryIndexes?: DynamoDB.GlobalSecondaryIndexList;

  private primaryKey: keyof T & string;

  constructor(
    tableName: DynamoDB.TableName,
    primaryKeySchema: DynamoDB.KeySchema = primaryKeySchemaDefault,
    attributeDefinitions: DynamoDB.AttributeDefinitions = attributeDefinitionsDefault,
    globalSecondaryIndexes?: DynamoDB.GlobalSecondaryIndexList,
  ) {
    this.tableName = tableName;
    this.primaryKeySchema = primaryKeySchema;
    this.attributeDefinitions = attributeDefinitions;
    this.globalSecondaryIndexes = globalSecondaryIndexes;
    this.primaryKey = primaryKeySchema[0].AttributeName as keyof T & string;
  }

  /** Initiates the store. */
  init = async (updateIndexes = true) => {
    await upsertTable(
      this.tableName,
      this.primaryKeySchema,
      this.attributeDefinitions,
      this.globalSecondaryIndexes,
      updateIndexes,
    );
  };

  private keyMethod = async (id: string, method: 'get' | 'delete') => {
    const params = {
      TableName: this.tableName,
      Key: { [this.primaryKey]: id },
    };
    return docClient[method](params).promise();
  };

  /** Retrieves the value. Returns undefined, if it doesn't exist. */
  safeFetch = async (id: string): Promise<T | undefined> => {
    const result = await this.keyMethod(id, 'get') as PromiseResult<DynamoDB.DocumentClient.GetItemOutput, AWSError>;
    return result.Item as (T | undefined);
  };

  /**
   * Retrieves the value. Throws Error, if doesn't exist.
   * @throws {Error} If value doesn't exist.
   * */
  fetch = async (id: string): Promise<T> => {
    const result = await this.safeFetch(id);
    if (!result) throw new Error('Item not found');
    return result;
  };

  list = async (projectionExpression?: string): Promise<Array<T>> => scan({
    TableName: this.tableName,
    ProjectionExpression: projectionExpression,
  });

  listKeys = async (): Promise<Array<string>> => {
    const result = await this.list(this.primaryKey);
    // @ts-ignore
    return result.map((item) => item[this.primaryKey]);
  };

  /** Removes data from the store. */
  delete = async (id: string) => {
    await this.keyMethod(id, 'delete');
  };

  /** Stores the value */
  store = async (data: T) => {
    await docClient.put({
      TableName: this.tableName,
      Item: data,
    }).promise();

    return data;
  };

  // AWS limitation, see: https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchWriteItem.html
  BATCH_STORE_CHUNK_SIZE = 25;

  /** Stores all the values */
  batchStore = async (records: Array<T>) => {
    const batchProms = [];
    for (let i = 0; i < records.length; i += this.BATCH_STORE_CHUNK_SIZE) {
      const batchRecords = records.slice(i, i + this.BATCH_STORE_CHUNK_SIZE);
      const params = {
        RequestItems: {
          [this.tableName]: batchRecords.map((record: T) => ({
            PutRequest: { Item: record },
          })),
        },
      };
      batchProms.push(docClient.batchWrite(params).promise());
    }
    await Promise.all(batchProms);
  };

  /** Executes the query in the store */
  query = async (filterObj: DeepPartial<T>): Promise<Array<T>> => scan({
    TableName: this.tableName,
    ...createQueryExpressions(this.primaryKey, filterObj),
  });

  /** Executes the query in the store */
  queryFirst = async (filterObj: DeepPartial<T>): Promise<T | undefined> => {
    const results = await this.query(filterObj);
    return results.length > 0 ? results[0] : undefined;
  };

  /** Executes a query filtering by a partition key value in the store */
  queryByIndex = async (
    attribute: string,
    value: string,
  ): Promise<Array<T>> => {
    const index = this.globalSecondaryIndexes?.find(
      (ind) => ind.KeySchema.find(
        (key) => key.AttributeName === attribute,
      ),
    )?.IndexName;
    if (!index) throw new Error(`No global index for attribute ${attribute}`);

    return query({
      TableName: this.tableName,
      IndexName: index,
      KeyConditionExpression: `#${attribute} = :${attribute}`,
      ExpressionAttributeNames: { [`#${attribute}`]: attribute },
      ExpressionAttributeValues: { [`:${attribute}`]: value },
    });
  };

  /** Executes a query filtering by a partition key value in the store */
  queryBy = async (attribute: keyof T, value: any): Promise<Array<T>> => this.query({
    [attribute]: value,
  } as DeepPartial<T>);

  /** Updates or creates the value. */
  upsert = async (id: string, data: DeepPartial<T>, opts: UpsertOptions = { returnValues: 'ALL_NEW' }): Promise<T> => {
    const params = {
      TableName: this.tableName,
      Key: { [this.primaryKey]: id },
      ...createUpdateExpressions(this.primaryKey, data),
      ReturnValues: opts.returnValues,
    };
    const response = await docClient.update(params).promise();
    return response.Attributes as T;
  };

  removeProps = async (id: string, data: DeepMap<T, boolean>): Promise<T> => {
    const params = {
      TableName: this.tableName,
      Key: { [this.primaryKey]: id },
      ...createRemovePropsExpressions(this.primaryKey, data),
      ReturnValues: 'ALL_NEW',
    };
    const response = await docClient.update(params).promise();
    return response.Attributes as T;
  };

  /**
   * Appends the `value` to the selected attribute as long as it is an array.
   * @throws {Error} if there is nothing stored at `id` or if it has something different
   * than an array.
   */
  appendValue = async <K>(id: string, attr: keyof T, value: K): Promise<T> => {
    const obj = await this.fetch(id);
    if (obj[attr] && !Array.isArray(obj[attr])) {
      throw new Error('Cannot append to non-array value');
    }
    const oldValues = (obj[attr] || []) as Array<K>;
    const newValues = [...oldValues, value];
    // @ts-ignore, I don't know how to check this type
    return this.upsert(id, { [attr]: newValues });
  };

  incrementValues = async (id: string, increments: DeepMap<T, number>): Promise<T> => {
    const params = {
      TableName: this.tableName,
      Key: { [this.primaryKey]: id },
      ...createIncrementValuesExpressions(this.primaryKey, increments),
      ReturnValues: 'ALL_NEW',
    };
    const response = await docClient.update(params).promise();
    return response.Attributes as T;
  };
}
