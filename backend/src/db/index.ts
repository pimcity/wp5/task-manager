import DynamoDB from 'aws-sdk/clients/dynamodb';

import {
  Task, Referrer, Referee, Sweepstake, WhitelistedUser,
} from './models';
import DynamoStore from './DynamoStore';
import logger from '../utils/logger';

const createGlobalIndex = (attr: string): DynamoDB.GlobalSecondaryIndex => ({
  IndexName: `${attr}-index`,
  KeySchema: [
    { KeyType: 'HASH', AttributeName: attr },
  ],
  Projection: { ProjectionType: 'ALL' },
});

export const tasks = new DynamoStore<Task>('Tasks', undefined,
  [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'userId', AttributeType: 'S' },
  ],
  [createGlobalIndex('userId')]);

export const referrers = new DynamoStore<Referrer>('Referrers',
  [
    { AttributeName: 'code', KeyType: 'HASH' },
  ],
  [
    { AttributeName: 'code', AttributeType: 'S' },
    { AttributeName: 'userId', AttributeType: 'S' },
  ],
  [createGlobalIndex('userId')]);

export const referees = new DynamoStore<Referee>('Referees',
  [
    { AttributeName: 'userId', KeyType: 'HASH' },
  ],
  [
    { AttributeName: 'userId', AttributeType: 'S' },
    { AttributeName: 'referrerId', AttributeType: 'S' },
  ],
  [createGlobalIndex('referrerId')]);

export const sweepstakes = new DynamoStore<Sweepstake>('Sweepstakes');

export const whitelistedUsers = new DynamoStore<WhitelistedUser>('WhitelistedUsers');

export const initStores = async () => {
  logger.info('Initializing stores...');

  await Promise.all([
    tasks.init(),
    referrers.init(),
    referees.init(),
    sweepstakes.init(),
    whitelistedUsers.init(),
  ]);

  logger.info('Stores initialized!');
};
