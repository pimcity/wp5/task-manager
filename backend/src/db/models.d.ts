/* **********
 * Models
 * ********** */

export type Periodicity = 'one-time' | 'daily' | 'weekly' | 'monthly' | 'yearly';

export interface WhitelistedUser {
  id: string;
}

export interface Referrer {
  code: string;
  userId: string;
}

export interface Referee {
  userId: string;
  code: string;
  referrerId: string;
  referralActivated?: boolean;
}

export interface AvailableTask {
  taskId: string;
  points: number;
  periodicity: Periodicity;
  maxRepetitions: number | 'unlimited';
  disabled?: boolean;
}

export interface Task {
  id: string; // `${taskId}-${userId}`
  taskId: string;
  userId: string;
  completedAt?: number;
  repetitions: number;
}

export interface SweepstakeInfo {
  id: string;
  description: string;
  prize: string;
  amount: number;
  pointsNeeded: number;
  periodicity: Periodicity;
  nextExecution?: number;
  disabled?: boolean;
}

export interface SweepstakeInit extends SweepstakeInfo {
  resetExecution?: boolean;
}

export interface Winner {
  executionDate: number;
  userIds: Array<string>;
}

export interface Sweepstake extends SweepstakeInfo {
  winners: Array<Winner>;
}
