/* eslint-disable */
"use strict";

import dotenv from 'dotenv';

Object.defineProperty(exports, "__esModule", {
  value: true
});

dotenv.config({ path: process.env.ENV_PATH || '.env' })

const { env } = process;

const config = {
  app: {
    name: env.npm_package_name,
    version: env.npm_package_version
  },
  env: env.NODE_ENV,
  debug: env.DEBUG === 'true',
  port: env.PORT,
  host: env.HOST,
  log: {
    error: env.ERROR_LOG,
    combined: env.COMBINED_LOG,
  },
  aws: {
    dynamo: {
      region: env.AWS_DYNAMO_REGION,
      accessKeyId: env.AWS_DYNAMO_ACCESS_KEY_ID,
      secretAccessKey: env.AWS_DYNAMO_SECRET_KEY_ID,
      endpoint: env.AWS_DYNAMO_ENDPOINT,
    },
  },
  security: {
    apiKeyHashes: JSON.parse(env.API_KEY_HASHES as string) as Array<string>,
    keycloak: {
      clientId: env.KEYCLOAK_CLIENT_ID as string,
      serverUrl: env.KEYCLOAK_SERVER_URL as string,
      realm: env.KEYCLOAK_REALM as string,
      realmPublicKey: env.KEYCLOAK_REALM_PUBLIC_KEY as string,
    },
  },
  services: {
    CreditsManager: {
      url: env.SERVICES_CREDITS_MANAGER_URL as string,
      apiKey: env.SERVICES_CREDITS_MANAGER_API_KEY as string,
    },
    PDS: {
      url: env.SERVICES_PDS_URL as string,
      clientId: env.SERVICES_PDS_CLIENT_ID as string,
      clientSecret: env.SERVICES_PDS_CLIENT_SECRET as string,
    },
  },
  referrals: {
    pointsThreshold: Number(env.REFERRALS_POINTS_THRESHOLD),
  }
};

export default config;
