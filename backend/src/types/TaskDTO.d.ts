import { Periodicity } from '../db/models';

export interface TaskDTO {
  id: string;
  name: string;
  description: string;
  completed: boolean;
  completedAt?: number;
  points: number;
  periodicity: Periodicity;
  repetitions: number;
  maxRepetitions: number | 'unlimited';
}
