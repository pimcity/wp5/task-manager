import { Periodicity } from '../db/models';

export interface SweepstakeDTO {
  pointsNeeded: number;
  previousDate?: string;
  date?: string;
  won?: boolean;
  lastWinner?: string;
  id: string;
  description: string;
  prize: string;
  amount: number;
  periodicity: Periodicity;
}
