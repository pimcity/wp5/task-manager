import app from './app';
import config from './config';
import logger from './utils/logger';

import { initStores } from './db';
import { populateSweepstakes } from './services/sweepstakes';

interface KeyValue<T> {
  [key: string]: T;
}

const checkConfig = (conf: KeyValue<any>) => {
  let error = false;
  const traverse = (o: KeyValue<any>, p?: string) => {
    Object.entries(o).forEach(([k, v]) => {
      const keyName = p ? `${p}.${k}` : k;
      if (v === undefined) {
        error = true;
        logger.error(`Environment variable '${keyName}' is not defined`);
      } else if (typeof v === 'object') {
        traverse(v, keyName);
      }
    });
  };
  traverse(conf);
  if (error) {
    logger.error('Configuration check failed. Exiting');
    process.exit(1);
  }
};

const server = async () => {
  checkConfig(config);
  await initStores();
  await populateSweepstakes();

  const {
    app: { name }, port, host, env,
  } = config;

  app.listen({ port, host }, () => logger.info(`${name} listening on port ${port} and host ${host} in ${env} mode`));
};

export default server;
