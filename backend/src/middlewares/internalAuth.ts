import crypto from 'crypto';
import { Request, Response, NextFunction } from 'express';

import config from '../config';
import { UnauthorizedError } from '../utils/error';

const hashApiKey = (apiKey: string) => crypto.createHash('sha256').update(apiKey).digest('hex');

const validateInternalAuthentication = (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const apiKeyHeader = req.headers['x-api-key'];
    if (!apiKeyHeader || apiKeyHeader.length === 0) {
      return next(new UnauthorizedError('Missing X-API-Key Header'));
    }

    // verify auth credentials
    const apiKey = Array.isArray(apiKeyHeader) ? apiKeyHeader[0] : apiKeyHeader;
    const sentApiKeyHash = hashApiKey(apiKey);
    if (config.security.apiKeyHashes.every((apiKeyHash) => apiKeyHash !== sentApiKeyHash)) {
      return next(new UnauthorizedError('Invalid API Key'));
    }

    return next();
  } catch (error) {
    const typedError = error as { message: string } | string;
    const errorMessage = typeof typedError === 'string' ? typedError : typedError.message;
    return next(new UnauthorizedError(errorMessage));
  }
};

export default validateInternalAuthentication;
