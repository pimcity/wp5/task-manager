import { Request, Response, NextFunction } from 'express';

import { AuthRequest } from '../types/middlewares/auth';

const decorateUserRequest = (req: Request, res: Response, next: NextFunction) => {
  const decoratedReq = req as AuthRequest;
  decoratedReq.user = { // @ts-ignore
    id: req.kauth.grant.access_token.content.sub,
    token: (req.headers.authorization as string).slice(7),
  };
  next();
};

export default decorateUserRequest;
