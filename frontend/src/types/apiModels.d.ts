export type Periodicity = 'one-time' | 'daily' | 'weekly' | 'monthly' | 'yearly';

export interface Winner {
  executionDate: number;
  userIds: Array<string>;
}

export interface Sweepstake {
  id: string;
  description: string;
  prize: string;
  amount: number;
  pointsNeeded: number;
  periodicity: Periodicity;
  nextExecution?: number;
  disabled?: boolean;
  winners: Array<Winner>;
}
