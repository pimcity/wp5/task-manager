import {
  useContext, createContext, ReactNode, useState,
} from 'react';
import Modal from '../components/Modal';

type ModalContext = React.Dispatch<React.SetStateAction<ReactNode>> | undefined;

const ModalContext = createContext<ModalContext>(undefined);

export const useModal = () => {
  const openModal = useContext<ModalContext>(ModalContext);
  if (!openModal) {
    throw new Error('Cannot use useModal outside a ModalProvider');
  }

  return {
    openModal,
    closeModal: () => openModal(undefined),
  };
};

export const ModalProvider = ({ children }: { children: ReactNode }) => {
  const [content, setContent] = useState<ReactNode | undefined>();
  return (
    <ModalContext.Provider value={setContent}>
      {children}
      <Modal isOpen={!!content} onClose={() => setContent(undefined)}>
        {content}
      </Modal>
    </ModalContext.Provider>
  );
};
