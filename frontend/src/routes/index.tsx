import Sweepstakes from '../screens/Sweepstakes';

import RoutesSkeleton from './RoutesSkeleton';

const Routes = () => (
  <RoutesSkeleton
    privateRoutes={[
      { path: '/sweepstakes', title: 'Sweepstakes', Component: Sweepstakes },
    ]}
  />
);

export default Routes;
