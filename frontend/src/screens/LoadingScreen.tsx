const LoadingScreen = () => (
  <div className="flex items-center justify-center w-screen h-screen">
    <div>Loading</div>
  </div>
);

export default LoadingScreen;
