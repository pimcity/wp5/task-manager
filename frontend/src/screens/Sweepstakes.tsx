/* eslint-disable react/display-name */
import Table from '../components/Table';
import apiRoutes from '../constants/apiRoutes.json';
import { useGet } from '../hooks/useApi';
import { Sweepstake } from '../types/apiModels';

import Actions from './components/Actions';

const sort = (a: Sweepstake, b: Sweepstake) => {
  if (!a.nextExecution && !b.nextExecution) return 0;
  if (!a.nextExecution) return 1;
  if (!b.nextExecution) return -1;
  return a.nextExecution - b.nextExecution;
};

const Sweepstakes = () => {
  const { data, isLoading, refetch } = useGet<Array<Sweepstake>>(apiRoutes.sweepstakes.list);
  return (
    <div>
      <Table
        isLoading={isLoading}
        sort={sort}
        data={data}
        columns={[
          {
            header: 'description',
            label: 'Name',
          },
          {
            header: 'prize',
            label: 'Prize',
          },
          {
            header: 'pointsNeeded',
            label: 'Points Needed',
          },
          {
            header: 'nextExecution',
            label: 'Next Date',
            displayFn: (nextExecution?: number) => <span>{
              nextExecution ? (new Date(nextExecution)).toISOString().substring(0, 10) : '-'
            }</span>,
          },
          {
            header: 'id',
            label: 'Actions',
            displayFn: (id: string, sweepstake) => (
              <Actions
                id={id}
                winners={sweepstake.winners}
                canExecute={!!sweepstake.nextExecution}
                refetch={refetch}
              />
            ),
          },
        ]}
      />
    </div>
  );
};

export default Sweepstakes;
