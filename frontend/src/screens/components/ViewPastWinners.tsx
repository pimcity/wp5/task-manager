/* eslint-disable react/display-name */
import { TrashIcon } from '@heroicons/react/outline';

import { useEffect } from 'react';

import Table from '../../components/Table';
import apiRoutes from '../../constants/apiRoutes.json';
import { useDelete } from '../../hooks/useApi';
import { Winner } from '../../types/apiModels';

import LoadingScreen from '../LoadingScreen';

interface ViewPastWinners {
  sweepstakeId: string;
  winners: Array<Winner>;
  onFinish: () => void;
}

const ViewPastWinners = ({ sweepstakeId, winners, onFinish }: ViewPastWinners) => {
  const { mutate: deleteWinners, status, error } = useDelete(
    `${apiRoutes.sweepstakes.winners}/${sweepstakeId}`,
  );

  useEffect(() => {
    if (status === 'success') onFinish();
  }, [status]);

  if (status === 'loading') return <LoadingScreen />;

  const lastExecution = winners[winners.length - 1]?.executionDate;

  return (
    <div className='flex flex-col'>
      <h3 className="mb-6 text-sm font-bold text-gray-700">Past Winners</h3>
      {!!error && <div className='mt-4 font-bold text-center text-red-500'>{error.message}</div>}
      <Table
        sort={(a, b) => b.executionDate - a.executionDate}
        columns={[
          {
            header: 'executionDate',
            label: 'Execution Date',
            displayFn: (executionDate: number) => <span>{
              (new Date(executionDate)).toISOString().substring(0, 10)
            }</span>,
          },
          {
            header: 'userIds',
            label: 'User ID',
            displayFn: (userIds: Array<string>, row) => (
              <div className='flex items-center justify-between'>
                <span>{userIds.join(', ')}</span>
                {row.executionDate === lastExecution && (
                  <TrashIcon
                    className='w-6 ml-3 text-gray-400 cursor-pointer hover:text-red-600'
                    aria-hidden='true'
                    onClick={() => deleteWinners({})}
                  />
                )}
              </div>
            ),
          },
        ]}
        data={winners}
      />
    </div>
  );
};

export default ViewPastWinners;
