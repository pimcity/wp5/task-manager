import AuthenticatedLink from '../../components/AuthenticatedLink';
import Button from '../../components/Button';
import apiRoutes from '../../constants/apiRoutes.json';
import { useModal } from '../../contexts/modalContext';
import { buildLink } from '../../services/api';
import { Winner } from '../../types/apiModels';

import SetWinners from './SetWinners';
import ViewPastWinners from './ViewPastWinners';

interface Actions {
  id: string;
  winners: Array<Winner>;
  canExecute: boolean;
  refetch: () => Promise<any>;
}

const Actions = ({
  id, winners, canExecute, refetch,
}: Actions) => {
  const { openModal, closeModal } = useModal();
  const participantsLink = buildLink(process.env, `${apiRoutes.sweepstakes.participants}/${id}`);

  const onFinish = async (submit: boolean) => {
    closeModal();
    if (submit) await refetch();
  };
  const setWinnersOnClick = () => openModal(<SetWinners sweepstakeId={id} onFinish={onFinish} />);

  const viewPastWinnersOnClick = () => openModal(
    <ViewPastWinners sweepstakeId={id} winners={winners} onFinish={() => onFinish(true)} />,
  );

  return (
    <div className="flex justify-evenly">
      {canExecute && (
        <>
          <AuthenticatedLink url={participantsLink}>View Participants</AuthenticatedLink>
          <Button onClick={setWinnersOnClick}>Set Winner</Button>
        </>
      )}
      <Button onClick={viewPastWinnersOnClick}>View Past Winners</Button>
    </div>
  );
};

export default Actions;
