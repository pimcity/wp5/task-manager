import { FormEvent, useEffect, useRef } from 'react';

import Button from '../../components/Button';
import apiRoutes from '../../constants/apiRoutes.json';
import { usePost } from '../../hooks/useApi';

import LoadingScreen from '../LoadingScreen';
import FormField from '../../components/FormField';

interface SetWinners {
  sweepstakeId: string;
  onFinish: (submit: boolean) => void;
}

const SetWinners = ({ sweepstakeId, onFinish }: SetWinners) => {
  const { mutate: submitWinners, status, error } = usePost<{ winners: Array<string>; }>(
    `${apiRoutes.sweepstakes.winners}/${sweepstakeId}`,
  );
  const refWinner = useRef<HTMLInputElement>(null);

  const submit = (e: FormEvent) => {
    e.preventDefault();
    if (!refWinner.current?.value) {
      return;
    }
    submitWinners({
      body: {
        winners: [refWinner.current?.value],
      },
    });
  };

  useEffect(() => {
    if (status === 'success') onFinish(true);
  }, [status]);

  if (status === 'loading') return <LoadingScreen />;

  return (
    <div className='flex flex-col'>
      <form onReset={() => onFinish(false)} onSubmit={submit}>
        <FormField
          inputRef={refWinner}
          label='Winner'
          title='PIMCity User ID'
          required
          placeholder="a9df82a7-48a9-4bbb-b387-8b05e8cca5d8"
          pattern="^[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}$"
        />
        {/* footer */}
        <div className="flex items-center justify-end pt-6">
          <Button type='reset' styleType='link' theme='cancel' className='mr-4'>Cancel</Button>
          <Button type='submit'>Submit</Button>
        </div>
      </form>
      {!!error && <div className='mt-4 font-bold text-center text-red-500'>{error.message}</div>}
    </div>
  );
};

export default SetWinners;
