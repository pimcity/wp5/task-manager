import { useKeycloak } from '@react-keycloak/web';
import Button from './Button';

const TopBar = ({ title }: { title: string }) => {
  const { keycloak } = useKeycloak();

  return (
      <nav className='flex items-center justify-between w-full px-8 py-3 shadow-lg' style={{ backgroundColor: '#f5f8fa' }}>
        <h1 className='text-lg font-semibold'>{title}</h1>
        <Button theme='cancel' onClick={() => keycloak.logout()}>Sign out</Button>
      </nav>
  );
};

export default TopBar;
