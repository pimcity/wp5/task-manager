import { DetailedHTMLProps, RefObject } from 'react';

interface FormFieldProps extends DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement
> {
  label?: string;
  inputRef: RefObject<HTMLInputElement>;
}

const FormField = ({
  inputRef, label, className, ...props
}: FormFieldProps) => (
  <div className={`flex flex-col ${className || ''}`}>
    {label && (
      <label className="mb-6 text-sm font-bold text-gray-700">
        {label}
      </label>
    )}
    <input
      className="w-full px-3 py-2 leading-tight text-gray-700 border rounded appearance-none focus:outline-none focus:shadow-outline"
      {...props}
      ref={inputRef}
    />
  </div>
);

export default FormField;
