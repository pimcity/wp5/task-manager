import {
  AnchorHTMLAttributes, createRef, useEffect, useState,
} from 'react';
import { useKeycloak } from '@react-keycloak/web';

export interface AuthenticatedLinkProps extends React.DetailedHTMLProps<
  AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement
> {
  url: string;
  theme?: 'primary' | 'secondary'
}

const themeClasses = {
  primary: 'bg-primary-600 hover:bg-primary-700 focus:ring-primary-600',
  secondary: 'bg-secondary-600 hover:bg-secondary-700 focus:ring-secondary-600',
};

interface FetchResponse {
  status: 'idle' | 'loading' | 'success';
  blob?: Blob;
}

const AuthenticatedLink = ({
  url, theme = 'primary', children, ...props
}: AuthenticatedLinkProps) => {
  const [response, setResponse] = useState<FetchResponse>({ status: 'idle' });
  const link = createRef<HTMLAnchorElement>();
  const { keycloak } = useKeycloak();

  useEffect(() => {
    if (response.status === 'success' && response.blob && (link.current && !link.current.href)) {
      const href = window.URL.createObjectURL(response.blob);

      link.current.href = href;
      link.current.target = '_blank';
      link.current.rel = 'noreferrer';

      link.current.click();
    }
  }, [response.status, link.current]);

  const handleAction = async () => {
    if (response.status === 'success') return;
    setResponse({ status: 'loading' });
    const token = keycloak?.token;
    const result = await fetch(url, {
      headers: { Authorization: token ? `Bearer ${token}` : '' },
    });
    setResponse({ status: 'success', blob: await result.blob() });
  };

  return (
    <a
      {...props}
      className={`inline-flex items-center px-3 py-3 font-medium text-white border border-transparent shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 transition duration-300 ease-in-out leading-4 rounded-md ${themeClasses[theme]}`}
      role='button'
      ref={link}
      onClick={handleAction}
    >
      {response.status === 'loading' ? 'Loading...' : children}
    </a>
  );
};

export default AuthenticatedLink;
