import { DetailedHTMLProps } from 'react';

interface BoxProps extends DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  heading?: any;
}

const Box = ({ children, heading, ...props }: BoxProps) => (
  <div
    {...props}
    className={`shadow-lg bg-white rounded-lg border border-gray-100 ${props.className || ''}`}
  >
    {heading && (
      <div className='px-6 py-5 font-medium border-b border-gray-100 text-md'>
        {typeof heading === 'string' ? <h2>{heading}</h2> : heading}
      </div>
    )}
    <div className='px-6 py-5'>
      {children}
    </div>
  </div>
);

export default Box;
