/* eslint-disable @typescript-eslint/ban-ts-comment */
interface Column<T> {
  header?: keyof T;
  label: string;
  className?: string;
  displayFn?: (value: any, row: T, rowIndex: number) => string | JSX.Element;
}

interface TableProps<T = unknown> {
  data?: Array<T>;
  columns: Array<Column<T>>;
  isLoading?: boolean;
  sort?: (a: T, b: T) => number;
}

const Table = <T extends Record<string, any>>({
  data, columns, isLoading, sort,
}: TableProps<T>) => {
  const rows = sort ? data?.sort(sort) : data;
  return (
    <div className="overflow-hidden border border-gray-100 rounded-lg">
      <table className="min-w-full text-sm divide-y divide-gray-100">
        <thead>
          <tr>
            {columns.map((col, index) => (
              <th
                key={col.header ? col.header as string : index}
                scope="col"
                className='px-4 py-5 font-medium tracking-wider text-left'
              >
                {col.label}
              </th>))
            }
          </tr>
        </thead>
        <tbody>
          {isLoading && (
            <tr>
              <td colSpan={columns.length}>Loading</td>
            </tr>
          )}
          {rows?.map((row, rowIndex) => (
            <tr key={rowIndex} style={{ backgroundColor: rowIndex % 2 === 0 ? 'white' : '#f5f8fa' }}>
              {columns.map((col, colIndex) => (
                <td key={colIndex} className={`px-4 py-4 ${col.className || ''}`}>
                  {/* @ts-ignore */}
                  {col.displayFn ? col.displayFn(row[col.header], row, rowIndex) : row[col.header]}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table >
    </div>
  );
};

export default Table;
