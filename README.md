Task Manager (TM)
===================

# Introduction

TODO

# Installation

The documentation and the following instructions refer to a Linux environment, running with **Docker Engine v20.10.x** and **Docker Compose: v1.27.x**. The TM project has been cloned from [this GitLab repository](https://gitlab.com/pimcity/wp5/task-manager.git).

Follow accurately the next steps to quickly set-up the TM backbone on your server. All relevant steps are designed for a Linux machine, perform the equivalent procedure with other environments.

1. Prepare the environment:

```
> sudo apt-get update
> sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
> curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
> echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
> sudo apt-get update
> sudo apt-get install docker-ce docker-ce-cli containerd.io
```

2. Import the project from the GIT repository:

```
> git clone https://gitlab.com/pimcity/wp5/task-manager.git
```

# Usage
## Execution
To run the TM service, the following command should be executed at the root of the repository:
```
> docker-compose up
```

## Configuration
The TM can be configured in three similar ways:

1. Using environment variables.
2. Defining those environment variables in a file called “.env” at the root directory of the folder with the PDK deployed, declaring them in the same way a variable is defined in the UNIX shell. You can check [this example](.env.example).
3. Modifying [docker-compose.yml](docker-compose.yml) file.

## Usage Examples
You can check [here](https://easypims.pimcity-h2020.eu/tm/api-docs/) the Swagger OpenAPI definition to see how the API is defined and used. Examples are provided as well.

## Authentication
KeyCloak service is used to carry out the authentication process. The user will not have to log in directly to the TM, but provide a JWT obtained from the authentication service or an EasyPIMS component, such as the PDA.

# License

The TM is distributed under AGPL-3.0-only, see the [LICENSE](LICENSE) file.

Copyright (C) 2021  Wibson - Daniel Fernandez, Rodrigo Irarrazaval
